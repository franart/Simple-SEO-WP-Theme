<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Simple_SEO
 * @since Simple SEO 1.0
 */
?>


<?php if( get_theme_mod( 'hide_copyright' ) == '') { ?>
    <footer><?php esc_attr_e('©', 'responsive'); ?><?php _e(date('Y')); ?>&nbsp;<a href="<?php echo home_url('/') ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"><?php echo get_theme_mod( 'copyright_textbox', bloginfo('name') ); ?></a></footer>
<?php } // end if ?>
        </div><!--Container-->
	</div><!--Wrapper-->
  </body>
</html>
