<!DOCTYPE html>
<html lang="<?php echo get_theme_mod( 'define_language' ); ?>" prefix="og: http://ogp.me/ns#">
  <head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <?php if (get_theme_mod( "add_noodp" ) == '1') : echo '<meta name=”robots” content=”noodp” />'; else: echo ''; endif; ?>
    <?php echo print_meta_nofollow()."\n"; ?>
    <link rel="alternate" hreflang="<?php echo get_theme_mod( 'define_language' ); ?>" href="<?php echo site_url( $path, $scheme ); ?>" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php echo site_url( $path, $scheme ); ?>/xmlrpc.php" />
    <title><?php single_post_title(); ?></title>
	<?php add_style( get_theme_mod( 'opc_stylesheet' ) ); ?>  
    <?php wp_head(); ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        
        ga('create', '<?php if (get_theme_mod( "define_google_analytics" )) : echo get_theme_mod( "define_google_analytics"); else: echo 'UA-XXXXXXX-X'; endif; ?>', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="https://apis.google.com/js/platform.js" async defer>
      {lang: 'es'}
    </script>
  </head>
  <body>
    <div class="wrapper">
	<div class="container">
        <header>
          <!-- nombre del sitio -->
          <p class="site-name"><?php bloginfo('name'); ?></p>
          <!-- descripción del sitio -->
          <p class="site-description"><?php echo get_bloginfo('description'); ?></p>
        </header>
        
       <!-- barra de navegación -->
	   <nav class="navbar navbar-default">
	      <ul class="main-nav">
             <?php wp_nav_menu( array( 'theme_location' => 'navegation' ) ); ?>
           </ul>
        </nav>