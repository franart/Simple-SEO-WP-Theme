<?php
/**
 * Simple SEO functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Simple_SEO
 * @since Simple SEO 1.0
 */

/**
 * Crear nuestros menús gestionables desde el
 * administrador de Wordpress.
 */
function mis_menus() {
  register_nav_menus(
    array(
      'navegation' => __( 'Menú de navegación' ),
    )
  );
}

/**
 * función para crear el widget derecho
 */
function mis_widgets(){
 register_sidebar(
   array(
       'name'          => __( 'Sidebar' ),
       'id'            => 'sidebar',
       'before_widget' => '<div class="widget">',
       'after_widget'  => '</div>',
       'before_title'  => '<h3>',
       'after_title'   => '</h3>',
   )
 );
}

/**
 * función para obtener el permalink sin el dominio
 */
function get_relative_permalink() {
    echo substr(get_permalink(), strlen(get_option('home')));
}

/**
 * Adds the individual sections, settings, and controls to the theme customizer
*/

function hide_elements_custom( $wp_customize ) {
    $wp_customize->add_section(
        'section_element_custom',
        array(
            'title' => 'Elementos visibles',
            'description' => 'En esta sección podrás personalizar los elementos que se mostraran/ocultaran en el sitio.',
            'priority' => 35,
        )
    );
    
    // ===== copyright ===== //
    $wp_customize->add_setting(
        'hide_copyright'
    );

    $wp_customize->add_control(
        'hide_copyright',
        array(
            'type' => 'checkbox',
            'label' => 'Ocultar copyright',
            'section' => 'section_element_custom'
        )
    );
    
    // ===== sidebar ===== //
    $wp_customize->add_setting(
        'hide_sidebar'
    );
    
    $wp_customize->add_control(
        'hide_sidebar',
        array(
            'type' => 'checkbox',
            'label' => 'Ocultar sidebar',
            'section' => 'section_element_custom'
        )
    );
    
    // ===== comments ===== //
    $wp_customize->add_setting(
        'hide_comments'
    );
    
    $wp_customize->add_control(
        'hide_comments',
        array(
            'type' => 'checkbox',
            'label' => 'Ocultar comentarios',
            'section' => 'section_element_custom'
        )
    );
}

add_action( 'customize_register', 'hide_elements_custom' );

/**
 * Configure SEO parameters
*/

function seo_section_custom( $wp_customize ) {
    $wp_customize->add_section(
        'seo_parameters_custom',
        array(
            'title' => 'Parámetros SEO',
            'description' => 'Sección de optimización SEO',
            'priority' => 40,
        )
    );
    
    
   // ===== language ===== //
    $wp_customize->add_setting(
        'define_language'
    );
    
    $wp_customize->add_control(
        'define_language',
        array(
            'type' => 'text',
            'label' => __( 'Idioma', 'textdomain' ),
            'section' => 'seo_parameters_custom',
            'description' => 'Para definir un idioma colocar su identificador, por ejemplo Español se debe colocar como "es", Inglés como "en" sin las comillas.',
        )
    ); 
    
   // ===== GA Tracking Code ===== //
    $wp_customize->add_setting(
        'define_google_analytics'
    );
    
    $wp_customize->add_control(
        'define_google_analytics',
        array(
            'type' => 'text',
            'label' => __( 'ID de Google Analytics', 'textdomain' ),
            'section' => 'seo_parameters_custom',
            'description' => 'Introducir en formato similar a UA-XXXXXX-X',
        )
    ); 
    
    // ===== noodp ===== //
    $wp_customize->add_setting(
        'add_noodp'
    );

    $wp_customize->add_control(
        'add_noodp',
        array(
            'type' => 'checkbox',
            'label' => __( 'Agregar meta etiqueta Noodp', 'textdomain' ),
            'section' => 'seo_parameters_custom',
            'description' => 'Con esta etiqueta los motores de búsqueda evitan usar el título y la descripción de Open Directory Projet (DMOZ) en las SERPs.',
        )
    );

    // ===== nofollow in comments ===== //
    $wp_customize->add_setting(
        'opc_nofollow'
    );
    
    $wp_customize->add_control(
        'opc_nofollow',
        array(
            'type' => 'checkbox',
            'label' => 'Agregar nofollow a URL de comentarios',
            'section' => 'seo_parameters_custom',
            'description' => 'Con está opción se puede agregar o quitar el atributo "nofollow" a los enlaces que se agreguen en los comentarios de cada post.',
        )
    );
	
	// ===== stylesheet in head ===== //
    $wp_customize->add_setting(
        'opc_stylesheet'
    );
    
    $wp_customize->add_control(
        'opc_stylesheet',
        array(
            'type' => 'checkbox',
            'label' => 'Carga hoja de estilos por tag link',
            'section' => 'seo_parameters_custom',
            'description' => 'Con está opción se agrega la carga de la hoja de estilos en el   head  del documento html por medio del tag link.',
        )
    );
    
}


add_action( 'customize_register', 'seo_section_custom' );

/**
 * Ejecución de funciones
 */
add_action( 'init', 'mis_menus' );
add_action('init','mis_widgets');
add_theme_support( 'post-thumbnails' );

/**
 * Widget::Remove/Add <meta> nofollow in <head> 
*/

add_action( 'init', 'cyb_register_meta_fields' );
function cyb_register_meta_fields() {
  register_meta( 'post', 'fnc_nofollow', 'fnc_sanitize_meta_nofollow', 'cyb_custom_fields_auth_callback' ); 
}

function fnc_sanitize_meta_nofollow( $value ) {
  if( ! empty( $value ) ) {
    return 1;
  } else {
    return 0;
  }
}

function cyb_custom_fields_auth_callback( $allowed, $meta_key, $post_id, $user_id, $cap, $caps ) {
    if( 'post' == get_post_type( $post_id ) && current_user_can( 'edit_post', $post_id ) ){
        $allowed = true;
    }
    else{
        $allowed = false;
    }
  return $allowed;
}

add_action('add_meta_boxes', 'cyb_meta_boxes');
function cyb_meta_boxes() {
    $post_types = array ( 'post', 'page' );
    foreach( $post_types as $post_type )
    {
        add_meta_box(
                     'cyb-meta-box',
                      __('Etiquetas para html'),
                      'cyb_meta_box_callback',
                      $post_type,
                      'side',
                      'high',
                       array( 'key' => 'value' )
        );
    }
}

function cyb_meta_box_callback( $post ) {
     wp_nonce_field( 'cyb_meta_box', 'cyb_meta_box_noncename' );
     $post_meta = get_post_custom( $post->ID );
    $current_value = '';
    if( isset( $post_meta['fnc_nofollow'][0] ) ) {
        $current_value = $post_meta['fnc_nofollow'][0];
    }
    ?>
    <p>
        <input type="checkbox" name="fnc_nofollow" id="fnc_nofollow" value="1" <?php checked( $current_value, 1 ); ?>> 
        
        <label class="label" for="fnc_nofollow"><?php  _e("nofollow", 'cyb_textdomain'); ?></label>
   </p>
   <?php
}

add_action( 'save_post', 'cyb_save_custom_fields', 10, 2 );
function cyb_save_custom_fields( $post_id, $post ){
    if ( ! isset( $_POST['cyb_meta_box_noncename'] ) || ! wp_verify_nonce( $_POST['cyb_meta_box_noncename'], 'cyb_meta_box' ) ) {
        return;
    }
    if( isset( $_POST['fnc_nofollow'] ) && $_POST['fnc_nofollow'] == "1" ) {
        update_post_meta( $post_id, 'fnc_nofollow', $_POST['fnc_nofollow'] );
    } else {
        delete_post_meta( $post_id, 'fnc_nofollow' );
    }
}

function print_meta_nofollow(  ){
    $custom_fields = get_post_custom();
    if( isset( $custom_fields['fnc_nofollow'] ) && $custom_fields['fnc_nofollow'] ) {
         return '<meta name="robots" content="noindex,nofollow">';
    }
    
}
/**
 * Remove/Add rel="nofollow" in comments
*/

add_filter('comment_text', function( $url ){
    $opc = get_theme_mod( 'opc_nofollow' ) == '' ? '0':'1';
    switch( $opc ){
        case '0':
            //elimina
            $url = str_ireplace(' rel="nofollow"', '', $url);
            return $url;
        break;
        case '1':
            //agrega
            $url = str_ireplace('">', '" rel="nofollow">', $url);
            return $url;
        break;
    }
    
} );

/**
 * Remove/Add Stylesheet file
*/

function add_style($opc){
    if( $opc == ''){ 
		$url = get_template_directory_uri();
		$fileContents = preg_replace('!/\*.*?\*/!s', '', file_get_contents($url.'/style.css'));
		echo "<style>".$fileContents."</style>";
	}else{
		$url = get_template_directory_uri();
		$final = '<link rel="stylesheet" href="'.$url.'/style.css">';
		echo $final;
	}
}


/**
 * Configure Social parameters
*/

add_action( 'fb_app_id', 'echo_fb_app_id' );
function echo_fb_app_id() {
	$url = site_url();
	$find = array( 'http://', 'https://' );
	$replace = '';
	$output = str_replace( $find, $replace, $url );
	$likept1 = '<iframe src="https://www.facebook.com/plugins/like.php?href=http%3A%2F%2F';
	$likept2 = '%2F&width=76&layout=button&action=like&show_faces=false&share=false&height=65&appId" width="76" height="65" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>';
	echo ($likept1.$output.$likept2);
}

add_action( 'tw_app_id_pt1', 'echo_tw_app_id_pt1' );
function echo_tw_app_id_pt1() {
	$tweetpt1 = '<iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button" title="Twitter Tweet Button" src="http://platform.twitter.com/widgets/tweet_button.6f0f2e104cd4fbbafb16bbad6813f68d.es.html#dnt=true&amp;id=twitter-widget-0&amp;lang=es&amp;original_referer=http%3A%2F%2F';
	echo $tweetpt1;
}

add_action( 'tw_app_id_pt2', 'echo_tw_app_id_pt2' );
function echo_tw_app_id_pt2() {
	$tweetpt2 = '%2F" style="position: static; visibility: visible; width: 71px; height: 20px;"></iframe>';
	echo $tweetpt2;
}

add_action( 'gplus_app_id', 'echo_gplus_app_id' );
function echo_gplus_app_id() {
    $gplus = '<div class="g-plus" data-action="share" data-annotation="none"></div>';
    echo $gplus;
}

function social_section_custom( $wp_customize ) {
    $wp_customize->add_section(
        'social_parameters_custom',
        array(
            'title' => 'Redes Sociales',
            'description' => 'Sección de Redes Sociales',
            'priority' => 45,
        )
    );
    
    // ===== facebook like ===== //
    $wp_customize->add_setting(
        'add_fblike'
    );

    $wp_customize->add_control(
        'add_fblike',
        array(
            'type' => 'checkbox',
            'label' => __( 'Agregar botón de like y compartir de Facebook', 'textdomain' ),
            'section' => 'social_parameters_custom',
        )
    );
    
	// ===== tweet button ===== //
    $wp_customize->add_setting(
        'add_twbutton'
    );

    $wp_customize->add_control(
        'add_twbutton',
        array(
            'type' => 'checkbox',
            'label' => __( 'Agregar botón de compartir en Twitter', 'textdomain' ),
            'section' => 'social_parameters_custom',
        )
    );

    // ===== google+ button ===== //
    $wp_customize->add_setting(
        'add_gplusbutton'
    );

    $wp_customize->add_control(
        'add_gplusbutton',
        array(
            'type' => 'checkbox',
            'label' => __( 'Agregar botón de compartir de Google+', 'textdomain' ),
            'section' => 'social_parameters_custom',
        )
    );
}

add_action( 'customize_register', 'social_section_custom' );


/**
 * función para eliminar script de emojis
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

?>
