<?php get_header(); ?>
    
  <div class="d-table">
    <!-- Contenido del post -->
    <?php if ( have_posts() ) : the_post(); ?>
      <div class="post_content" <?php if (get_theme_mod( "hide_sidebar" ) == '1') : echo 'style="width: 100%;"'; else: echo ''; endif; ?>>
          <article>
            <header><h1><?php the_title(); ?></h1></header>
			     <div class="social_share">
              <div class="fb_button"><?php if (get_theme_mod( "add_fblike" ) == '1') : echo do_action( 'fb_app_id' ); else: echo ''; endif; ?></div>
              <div class="tw_button"><?php if (get_theme_mod( "add_twbutton" ) == '1') : echo do_action( 'tw_app_id_pt1' ); else: echo ''; endif; if (get_theme_mod( "add_twbutton" ) == '1') : echo do_action( 'tw_app_id_pt2' ); else: echo ''; endif;?></div>
              <div class="gplus_button"><?php if (get_theme_mod( "add_gplusbutton" ) == '1') : echo do_action( 'gplus_app_id' ); else: echo ''; endif; ?></div>
              </div>
            <time datatime="<?php the_time('Y-m-j'); ?>"><?php the_time('j F, Y'); ?></time>

              <?php 
              // Primero se chequea si hay asignada alguna imagen destacada.
                if ( has_post_thumbnail() ) {
                    the_post_thumbnail();
                } 
                the_content();
              ?>

              <address>Por <?php the_author_posts_link() ?></address>
			  <br>
			  <div class="social_share">
				  <div class="fb_button"><?php if (get_theme_mod( "add_fblike" ) == '1') : echo do_action( 'fb_app_id' ); else: echo ''; endif; ?></div>
				  <div class="tw_button"><?php if (get_theme_mod( "add_twbutton" ) == '1') : echo do_action( 'tw_app_id_pt1' ); else: echo ''; endif; if (get_theme_mod( "add_twbutton" ) == '1') : echo do_action( 'tw_app_id_pt2' ); else: echo ''; endif;?></div>
          <div class="gplus_button"><?php if (get_theme_mod( "add_gplusbutton" ) == '1') : echo do_action( 'gplus_app_id' ); else: echo ''; endif; ?></div>
				</div>
              <!-- Comentarios -->
              <?php if( get_theme_mod( 'hide_comments' ) == '') {
                        comments_template();
                    } // end if ?>
          </article>
         <?php else : ?>
          <p><?php _e('Ups!, esta entrada no existe.'); ?></p>
         <?php endif; ?>
     </div>
     <!-- Archivo de barra lateral por defecto -->
     <div class="widgets_bar">
        <?php get_sidebar(); ?>
     </div>
  </div>
      
<!-- Archivo de pié global de Wordpress -->
<?php get_footer(); ?>
