<!-- Archivo de cabecera global de Wordpress -->
<?php get_header(); ?>
<p class="no-found"> 
    La página no existe. <br/>
    <a href="<?php echo get_home_url(); ?>">Ir a la página de inicio</a>
</p>