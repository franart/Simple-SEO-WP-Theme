<!-- Archivo de cabecera global de Wordpress -->
<?php get_header(); ?>
<!-- Listado de posts --> 
<strong>Páginas</strong>
		<?php
		// Start the loop.
		$args = array(
			'sort_order' => 'asc',
			'sort_column' => 'post_title',
			'hierarchical' => 1,
			'exclude' => '',
			'include' => '',
			'meta_key' => '',
			'meta_value' => '',
			'authors' => '',
			'child_of' => 0,
			'parent' => -1,
			'exclude_tree' => '',
			'number' => '',
			'offset' => 0,
			'post_type' => 'page',
			'post_status' => 'publish'
		); 
		$pages = get_pages($args); 
			// End of the loop.
		?>
		<?php
		    $pages= get_pages();
	    ?>
		<ul>
		<?php
		    foreach ( $pages as $page ) {
		      if( get_the_title($page->ID) !== "Archivos" and get_the_title($page->ID) !== "Página de Inicio" ){   
            ?> 
		        <li><a href='<?php echo get_page_link( $page->ID ); ?>'><?php echo get_the_title($page->ID); ?></a></li> 
		        <?php
              }
		    }
		?>
		</ul>
        <br />
         <strong>Post</strong>
		<?php 
		// the query
		$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
		<?php if ( $wpb_all_query->have_posts() ) : ?>
			<ul class="post-list">
				<!-- the loop -->
				<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
				<?php endwhile; ?>
				<!-- end of the loop -->
			</ul>
		<?php wp_reset_postdata(); ?>
		<?php else : ?>
			<p><?php _e( 'Lo sentimos, no hay post para mostrar.' ); ?></p>
		<?php endif; ?>